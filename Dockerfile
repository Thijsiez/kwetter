FROM java:8

WORKDIR /

ADD build/libs/kwetter-0.0.1-SNAPSHOT.jar kwetter.jar

EXPOSE 8080

CMD java -jar kwetter.jar
