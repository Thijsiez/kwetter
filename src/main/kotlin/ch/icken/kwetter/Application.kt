package ch.icken.kwetter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

//This defines the main entry point of our application
@SpringBootApplication
class KwetterApplication

fun main(args: Array<String>) {
    runApplication<KwetterApplication>(*args)
}