package ch.icken.kwetter

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

inline fun <T> T.scope(block: (T) -> Unit): T {
    block(this); return this
}

inline fun <T> Iterable<T>.scopeForEach(block: (T) -> Unit) = forEach(block)

inline fun <E, R> List<E>.containsItemsElse(containsItems: (List<E>) -> R, notContainsItems: () -> R): R {
    return if (isEmpty()) notContainsItems() else containsItems(this)
}

inline fun <T> Optional<T>.scopeIfPresent(block: (T) -> Unit): Optional<T> {
    if (isPresent) get().scope(block); return this
}

inline fun <T> Optional<T>.scopeIfPresentElse(ifPresent: (T) -> Unit, ifNotPresent: () -> T): T {
    return if (isPresent) get().scope(ifPresent) else ifNotPresent()
}

inline fun <T, R> Optional<T>.ifPresentElse(ifPresent: (T) -> R, ifNotPresent: () -> R): R {
    return if (isPresent) ifPresent(get()) else ifNotPresent()
}

inline fun <T, R> List<Optional<T>>.ifAllPresentElse(ifAllPresent: (List<T>) -> R, ifNotAllPresent: () -> R): R {
    return if (all { it.isPresent }) ifAllPresent(map { it.get() }) else ifNotAllPresent()
}

fun <T> Sequence<Optional<T>>.filterIsPresent() = filter { it.isPresent }.map { it.get() }

fun <T, ID> JpaRepository<T, ID>.findByIds(vararg ids: ID) = ids.map { findById(it) }
fun <T, ID> JpaRepository<T, ID>.save(vararg entities: T) = entities.map { save(it) }

fun Date.subtractMinutes(amount: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(Calendar.MINUTE, -amount)
    return calendar.time
}