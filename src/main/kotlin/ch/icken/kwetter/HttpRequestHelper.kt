package ch.icken.kwetter

import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody

private val okhttpClient = OkHttpClient()

fun <R> httpGetRequest(url: HttpUrl, onSuccess: (body: String) -> R, noSuccess: (errorCode: Int) -> R): R =
        okhttpClient.newCall(Request.Builder().apply {
            url(url)
        }.build()).execute().let { response ->
            if (response.isSuccessful) {
                onSuccess(response.body()?.string() ?: "")
            } else {
                noSuccess(response.code())
            }
        }