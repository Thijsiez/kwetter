package ch.icken.kwetter

import org.ocpsoft.prettytime.PrettyTime
import java.util.*

val newId get() = UUID.randomUUID().toString()
val now get() = Date() //TODO implement a better time API
val prettyTime by lazyOf(PrettyTime())