package ch.icken.kwetter.controller

object ApiKeys {
    const val API_URL_PREFIX = "api"
    const val TYPE = "type"

    object User {
        const val TYPE = "user"
        const val ID = "userId"
        const val USERNAME = "username"
        const val FULL_NAME = "fullName"
        const val PROFILE_PICTURE = "profilePicture"
        const val DATE_OF_BIRTH = "dateOfBirth"
        const val JOIN_DATE = "joinDate"
        const val VERIFIED = "verified"
        const val BIOGRAPHY = "biography"
        const val KWEET_COUNT = "kweetCount"
        const val FOLLOWING_COUNT = "followingCount"
        const val FOLLOWER_COUNT = "followerCount"
    }

    object Kweet {
        const val TYPE = "kweet"
        const val ID = "kweetId"
        const val USER = "user"
        const val TIMESTAMP = "timestamp"
        const val CONTENT = "content"
        const val MENTIONS = "mentions"
        const val REPLY_TO_KWEET_ID = "replyToKweetId"
        const val REPLY_TO_USER = "replyToUser"
        const val REPLY_ROOT_KWEET_ID = "replyRootKweetId"
        const val REPLY_THREAD = "replyThread"
        const val QUOTES = "quotes"
        const val MEDIA = "media"
        const val REPLY_COUNT = "replyCount"
        const val REKWEET_COUNT = "rekweetCount"
        const val LIKE_COUNT = "likeCount"
    }

    object Rekweet {
        const val TYPE = "rekweet"
        const val REKWEETING_USER = "rekweetingUser"
        const val REKWEETED_KWEET = "rekweetedKweet"
        const val TIMESTAMP = "timestamp"
    }

    object Like {
        const val TYPE = "like"
        const val LIKING_USER = "likingUser"
        const val LIKED_KWEET = "likedKweet"
        const val TIMESTAMP = "timestamp"
    }

    object Timeline {
        const val TYPE = "timeline"
        const val TIMELINE = "timeline"
    }

    object Following {
        const val TYPE = "following"
        const val FOLLOWING = "following"
    }

    object Followers {
        const val TYPE = "followers"
        const val FOLLOWERS = "followers"
    }
}