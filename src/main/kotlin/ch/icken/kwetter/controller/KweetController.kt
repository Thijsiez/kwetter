package ch.icken.kwetter.controller

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.scopeIfPresentElse
import ch.icken.kwetter.service.KweetService
import ch.icken.kwetter.service.TokenService
import ch.icken.kwetter.service.UserService
import com.fasterxml.jackson.databind.node.ObjectNode
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
class KweetController {
    @Autowired
    private lateinit var kweetService: KweetService
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var tokenService: TokenService

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/kweet/{id}")
    fun getKweet(@PathVariable id: String): ResponseEntity<ObjectNode> {
        return kweetService.findById(id).ifPresentElse({ foundKweet ->
            ResponseEntity.ok(ResponseObjectBuilder.buildKweet(foundKweet))
        }, { ResponseEntity.notFound().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/kweet/create")
    fun createKweet(@RequestBody json: ObjectNode): ResponseEntity<ObjectNode> {
        val requiredKeys = arrayOf(ApiKeys.Kweet.USER, ApiKeys.Kweet.CONTENT)
        return if (requiredKeys.all { key -> json.has(key) }) {
            try {
                userService.findByUsername(json[ApiKeys.Kweet.USER].asText()).ifPresentElse({ foundUser ->
                    val kweet = Kweet(foundUser, json[ApiKeys.Kweet.CONTENT].asText())
                    ResponseEntity.ok(ResponseObjectBuilder.buildKweet(kweetService.save(kweet)))
                    //TODO: add other things to kweet (quote, media etc.)
                }, {
                    ResponseEntity.notFound().build()
                })
            } catch (e: Exception) {
                ResponseEntity.status(500).build<ObjectNode>()
            }
        } else {
            ResponseEntity.badRequest().build()
        }
    }

    @DeleteMapping("/${ApiKeys.API_URL_PREFIX}/kweet/{id}/remove")
    fun removeKweet(
            @PathVariable id: String,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return kweetService.findById(id).ifPresentElse({ foundKweet ->
            if(foundKweet.user.userId != userId)  return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
            val removedKweet = kweetService.removeKweet(foundKweet)
            ResponseEntity.ok().build()
        }, {
            //TODO: handle more gracefully
            throw NotFoundException("Kweet not found")
        })
    }

    //TODO: fix removeKweet
//    @DeleteMapping("/kweet/{id}/remove")
//    fun removeKweet(@PathVariable id: String): ResponseEntity<ObjectNode> {
//        return kweetService.findById(id).ifPresentElse({ foundKweet ->
//            kweetService.removeById(foundKweet.timelineableId)
//            ResponseEntity.ok().build()
//        }, { ResponseEntity.notFound().build() })
//    }
}