package ch.icken.kwetter.controller

import ch.icken.kwetter.domain.Media
import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.service.MediaService
import javassist.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.IOException

@RestController
class MediaController {
    @Autowired
    private lateinit var mediaService: MediaService

    private val dir: String = "uploads/" //TODO

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/file")
    @ResponseStatus(HttpStatus.CREATED)
    @Throws(IOException::class)
    fun uploadImage(@RequestParam("file") file: MultipartFile): Media {
        return this.mediaService.storeImage(file)
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/file/{mediaId}")
    @Throws(IOException::class)
    fun getImage(@PathVariable mediaId: String): Media {
        return mediaService.findById(mediaId).ifPresentElse({ media ->
            media
        },{
            throw NotFoundException("Media not found") //TODO
        })
    }
}