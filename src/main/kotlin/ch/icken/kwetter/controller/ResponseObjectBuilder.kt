package ch.icken.kwetter.controller

import ch.icken.kwetter.domain.*
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode

object ResponseObjectBuilder {
    private val mapper = ObjectMapper()

    fun buildUserLoginData(user: User): ObjectNode {
        val userNode = mapper.createObjectNode()

        userNode.put("username", user.username)
        userNode.put("userId", user.userId)

        return userNode
    }

    fun buildUserCard(user: User): ObjectNode {
        val userNode = mapper.createObjectNode()

        userNode.put(ApiKeys.TYPE, ApiKeys.User.TYPE)
        userNode.put(ApiKeys.User.ID, user.userId)
        userNode.put(ApiKeys.User.USERNAME, user.username)
        userNode.put(ApiKeys.User.FULL_NAME, user.fullName)
        userNode.put(ApiKeys.User.PROFILE_PICTURE, user.profilePicture?.url)
        userNode.put(ApiKeys.User.VERIFIED, user.verified)
        //TODO "youFollow" and "followsYou"
        userNode.put(ApiKeys.User.BIOGRAPHY, user.biography)
        //TODO kweetCount
        //TODO followingCount, user.following.size is too costly
        //TODO followerCount, user.followers.size is too costly

        return userNode
    }

    fun buildUserProfile(user: User): ObjectNode {
        val profileNode = buildUserCard(user)

        profileNode.put(ApiKeys.User.DATE_OF_BIRTH, user.dateOfBirth?.time) //TODO implement a better time API
        profileNode.put(ApiKeys.User.JOIN_DATE, user.joinDate.time) //TODO implement a better time API

        return profileNode
    }

    fun buildKweet(kweet: Kweet, topLevelObject: Boolean = true): ObjectNode {
        val kweetNode = mapper.createObjectNode()

        kweetNode.put(ApiKeys.TYPE, ApiKeys.Kweet.TYPE)
        kweetNode.put(ApiKeys.Kweet.ID, kweet.timelineableId)
        kweetNode.set(ApiKeys.Kweet.USER, buildUserCard(kweet.user))
        kweetNode.put(ApiKeys.Kweet.TIMESTAMP, kweet.timestamp.time) //TODO implement a better time API
        kweetNode.put(ApiKeys.Kweet.CONTENT, kweet.content)
        kweetNode.put("isDeleted", kweet.isDeleted)

        val mentionsUsernames = mapper.createArrayNode()
        kweet.mentions.forEach { mentionsUsernames.add(it.username) }
        kweetNode.set(ApiKeys.Kweet.MENTIONS, mentionsUsernames)

        if (kweet.replyTo != null) {
            kweetNode.put(ApiKeys.Kweet.REPLY_TO_KWEET_ID, kweet.replyTo.timelineableId)
            kweetNode.set(ApiKeys.Kweet.REPLY_TO_USER, buildUserCard(kweet.replyTo.user))
            kweetNode.put(ApiKeys.Kweet.REPLY_ROOT_KWEET_ID, kweet.replyRoot!!.timelineableId)
            kweetNode.put(ApiKeys.Kweet.REPLY_THREAD, kweet.replyThread)
        }

        if (topLevelObject) {
            val quotesKweets = mapper.createArrayNode()
            kweet.quotes?.forEach { quotesKweets.add(buildKweet(it, false)) }
            kweetNode.set(ApiKeys.Kweet.QUOTES, quotesKweets)
        }

        val mediaUrls = mapper.createArrayNode()
        kweet.media?.forEach { mediaUrls.add(it.url) }
        kweetNode.set(ApiKeys.Kweet.MEDIA, mediaUrls)

        //TODO replyCount
        //TODO rekweetCount
        //TODO likeCount

        return kweetNode
    }

    fun buildRekweet(rekweet: Rekweet): ObjectNode {
        val rekweetNode = mapper.createObjectNode()

        rekweetNode.put(ApiKeys.TYPE, ApiKeys.Rekweet.TYPE)
        rekweetNode.set(ApiKeys.Rekweet.REKWEETING_USER, buildUserCard(rekweet.user))
        rekweetNode.set(ApiKeys.Rekweet.REKWEETED_KWEET, buildKweet(rekweet.kweet))
        rekweetNode.put(ApiKeys.Rekweet.TIMESTAMP, rekweet.timestamp.time) //TODO implement a better time API

        return rekweetNode
    }

    fun buildLike(like: Like): ObjectNode {
        val likeNode = mapper.createObjectNode()

        likeNode.put(ApiKeys.TYPE, ApiKeys.Like.TYPE)
        likeNode.set(ApiKeys.Like.LIKING_USER, buildUserCard(like.user))
        likeNode.set(ApiKeys.Like.LIKED_KWEET, buildKweet(like.kweet))
        likeNode.put(ApiKeys.Like.TIMESTAMP, like.timestamp.time) //TODO implement a better time API

        return likeNode
    }

    fun buildTimeline(timelineables: List<Timelineable>): ObjectNode {
        val timelineNode = mapper.createObjectNode()

        val timelineablesNode = mapper.createArrayNode()
        timelineablesNode.addAll(timelineables.map { when (it) {
            is Kweet -> buildKweet(it)
            is Rekweet -> buildRekweet(it)
            is Like -> buildLike(it)
            else -> throw IllegalStateException("This is impossible")
        } })

        timelineNode.put(ApiKeys.TYPE, ApiKeys.Timeline.TYPE)
        timelineNode.set(ApiKeys.Timeline.TIMELINE, timelineablesNode)

        return timelineNode
    }

    fun buildFollowing(users: List<User>): ObjectNode {
        val followingNode = mapper.createObjectNode()

        val followingUsersNode = mapper.createArrayNode()
        followingUsersNode.addAll(users.map { buildUserCard(it) })

        followingNode.put(ApiKeys.TYPE, ApiKeys.Following.TYPE)
        followingNode.set(ApiKeys.Following.FOLLOWING, followingUsersNode)

        return followingNode
    }

    fun buildFollowers(users: List<User>): ObjectNode {
        val followersNode = mapper.createObjectNode()

        val followersUsersNode = mapper.createArrayNode()
        followersUsersNode.addAll(users.map { buildUserCard(it) })

        followersNode.put(ApiKeys.TYPE, ApiKeys.Followers.TYPE)
        followersNode.set(ApiKeys.Followers.FOLLOWERS, followersUsersNode)

        return followersNode
    }
}