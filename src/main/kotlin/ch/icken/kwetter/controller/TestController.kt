package ch.icken.kwetter.controller

import ch.icken.kwetter.domain.*
import ch.icken.kwetter.service.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class TestController {
    //region Repositories
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var kweetService: KweetService
    @Autowired
    private lateinit var rekweetService: RekweetService
    @Autowired
    private lateinit var likeService: LikeService
    @Autowired
    private lateinit var hashtagService: HashtagService
    @Autowired
    private lateinit var mediaService: MediaService
    //endregion

    @GetMapping("/test/insert_data")
    fun insertTestData() {
        userService.deleteAll()
        kweetService.deleteAll()
        rekweetService.deleteAll()
        likeService.deleteAll()
        hashtagService.deleteAll()

        val (thijs, dennis, sjors) = userService.save(
                User("Thijsiez", "Thijs Koppen", "thijsiez@hotmail.com"),
                User("DenizNavidad", "Dennis Koeter", "dennis.koeter@hotmail.com"),
                User("RapperSjors", "Rapper Sjors", "rapper@sjors.nl")
        )

        userService.startFollowing(thijs, dennis)
        userService.startFollowing(thijs, sjors)
        userService.save(thijs, dennis, sjors)

        val firstKweet = Kweet(sjors, "Ik heb net pindakaas gegeten")
        firstKweet.timestamp = dateForTime(14, 12)
        kweetService.save(firstKweet)

        val secondKweet = Kweet(thijs, "@DenizNavidad is veel cooler dan ik... #jealous")
        secondKweet.timestamp = dateForTime(16, 47)
        kweetService.save(secondKweet)
        kweetService.persistMentionsAndHashtags(secondKweet)
        kweetService.save(secondKweet)

        val thirdKweet = Kweet(dennis, "@Thijsiez Thanks pik!",
                replyTo = secondKweet, quotes = arrayOf(secondKweet))
        thirdKweet.timestamp = dateForTime(18, 12)
        kweetService.save(thirdKweet)
        kweetService.persistMentionsAndHashtags(thirdKweet)
        kweetService.save(thirdKweet)

        val like = Like(dennis, secondKweet)
        like.timestamp = dateForTime(18, 13)
        likeService.save(like)

        val rekweet = Rekweet(dennis, firstKweet)
        rekweet.timestamp = dateForTime(18, 21)
        rekweetService.save(rekweet)

        val fourthKweet = Kweet(dennis, "Dit is een kweet! #Kwetter")
        fourthKweet.timestamp = dateForTime(18, 37)
        kweetService.save(fourthKweet)
        kweetService.persistMentionsAndHashtags(fourthKweet)
        kweetService.save(fourthKweet)

        val media = Media("file", "https://resizing.flixster.com/xXEfQBYKKAa36NmRXGXnYCnuXck=/300x300/v1.bjsxMjIzNzM5O2o7MTc3NDQ7MTIwMDs2NTI7NDg5")
        mediaService.save(media)

        val fifthKweet = Kweet(dennis, "Dit is een media Kweet", false, null, null, arrayOf(media))
        fifthKweet.timestamp = dateForTime(18, 58)
        kweetService.save(fifthKweet)

        val sixthKweet = Kweet(thijs, "@DenizNavidad maar het is gewoon waar toch", replyTo = thirdKweet)
        sixthKweet.timestamp = dateForTime(19, 3)
        kweetService.save(sixthKweet)
        kweetService.persistMentionsAndHashtags(sixthKweet)
        kweetService.save(sixthKweet)
    }

    //TODO implement better time API
    private fun dateForTime(hour: Int, minute: Int): Date {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, hour)
        cal.set(Calendar.MINUTE, minute)
        return cal.time
    }
}