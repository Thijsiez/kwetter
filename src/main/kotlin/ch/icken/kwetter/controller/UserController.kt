package ch.icken.kwetter.controller

import ch.icken.kwetter.containsItemsElse
import ch.icken.kwetter.domain.*
import ch.icken.kwetter.httpGetRequest
import ch.icken.kwetter.ifAllPresentElse
import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.service.*
import com.fasterxml.jackson.databind.node.ObjectNode
import javassist.NotFoundException
import okhttp3.HttpUrl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
class UserController {
    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var kweetService: KweetService
    @Autowired
    private lateinit var likeService: LikeService
    @Autowired
    private lateinit var rekweetService: RekweetService
    @Autowired
    private lateinit var tokenService: TokenService

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/logindata")
    fun getUserLoginData(@PathVariable username: String): ResponseEntity<ObjectNode> {
        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            ResponseEntity.ok(ResponseObjectBuilder.buildUserLoginData(foundUser))
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/card")
    fun getUserCard(@PathVariable username: String,
                    @RequestHeader("Authorization") token: String,
                    @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            ResponseEntity.ok(ResponseObjectBuilder.buildUserCard(foundUser))
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/profile")
    fun getUserProfile(@PathVariable username: String,
                       @RequestHeader("Authorization") token: String,
                       @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            ResponseEntity.ok(ResponseObjectBuilder.buildUserProfile(foundUser))
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/timeline")
    fun getUserTimeline(
            @PathVariable username: String,
            @RequestParam(value = "page", required = false) page: Int?,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            userService.getTimeline(foundUser, page ?: 0).containsItemsElse({ foundKweets ->
                ResponseEntity.ok(ResponseObjectBuilder.buildTimeline(foundKweets))
            }, { ResponseEntity.noContent().build() })
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/timeline/{search}")
    fun getUserTimelineSearch(
            @PathVariable username: String,
            @PathVariable search: String,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            userService.getTimelineSearch(foundUser, search).containsItemsElse({ foundKweets ->
                val returnList = mutableListOf<Timelineable>()
                for(kweet in foundKweets){
                    if(kweet is Kweet){
                        if(kweet.content.contains(search)){
                            returnList.add(kweet)
                        }
                    }
                }
                ResponseEntity.ok(ResponseObjectBuilder.buildTimeline(returnList))
            }, { ResponseEntity.noContent().build() })
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/kweets")
    fun getUserKweets(
            @PathVariable username: String,
            @RequestParam(value = "page", required = false) page: Int?,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            userService.getKweets(foundUser, page ?: 0).containsItemsElse({ foundKweets ->
                ResponseEntity.ok(ResponseObjectBuilder.buildTimeline(foundKweets))
            }, { ResponseEntity.noContent().build() })
        }, { ResponseEntity.notFound().build() })
    }

    //TODO: maybe add mentions to this method also?
    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/kweets_and_replies")
    fun getUserKweetsAndReplies(
            @PathVariable username: String,
            @RequestParam(value = "page", required = false) page: Int?,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            userService.getKweetsAndReplies(foundUser, page ?: 0).containsItemsElse({ foundKweets ->
                ResponseEntity.ok(ResponseObjectBuilder.buildTimeline(foundKweets))
            }, { ResponseEntity.noContent().build() })
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/kweets_with_media")
    fun getUserKweetsWithMedia(
            @PathVariable username: String,
            @RequestParam(value = "page", required = false) page: Int?,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            userService.getKweetsWithMedia(foundUser, page ?: 0).containsItemsElse({ foundKweets ->
                ResponseEntity.ok(ResponseObjectBuilder.buildTimeline(foundKweets))
            }, { ResponseEntity.noContent().build() })
        }, { ResponseEntity.notFound().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/likes")
    fun getUserLikes(
            @PathVariable username: String,
            @RequestParam(value = "page", required = false) page: Int?,
            @RequestHeader("Authorization") token: String,
            @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            userService.getLikes(foundUser, page ?: 0).containsItemsElse({ foundLikes ->
                ResponseEntity.ok(ResponseObjectBuilder.buildTimeline(foundLikes))
            }, { ResponseEntity.noContent().build() })
        }, {
            ResponseEntity.notFound().build()
        })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/following")
    fun getUserFollows(@PathVariable username: String): ResponseEntity<ObjectNode> {
        return userService.findFollows(username).containsItemsElse({ foundFollows ->
            ResponseEntity.ok(ResponseObjectBuilder.buildFollowing(foundFollows))
        }, { ResponseEntity.noContent().build() })
    }

    @GetMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/followers")
    fun getUserFollowers(@PathVariable username: String): ResponseEntity<ObjectNode> {
        return userService.findFollowers(username).containsItemsElse({ foundFollowers ->
            ResponseEntity.ok(ResponseObjectBuilder.buildFollowers(foundFollowers))
        }, { ResponseEntity.noContent().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/follow/{followee}")
    fun followUser(@PathVariable username: String,
                   @PathVariable followee: String,
                   @RequestHeader("Authorization") token: String,
                   @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
        return userService.findByUsernames(username, followee).ifAllPresentElse({ (foundUser, foundFollowee) ->
            userService.startFollowing(foundUser, foundFollowee)

            val (savedUser, savedFollowee) = userService.save(foundUser, foundFollowee)
            if (userService.isFollowing(savedUser, savedFollowee)) {
                ResponseEntity.ok().build()
            } else {
                //TODO: rollback?
                //HTTP Status Code 500: Internal Server Error
                ResponseEntity.status(500).build()
            }
        }, { ResponseEntity.notFound().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/unfollow/{followee}")
    fun unfollowUser(@PathVariable username: String,
                   @PathVariable followee: String,
                   @RequestHeader("Authorization") token: String,
                   @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
        return userService.findByUsernames(username, followee).ifAllPresentElse({ (foundUser, foundFollowee) ->
            userService.stopFollowing(foundUser, foundFollowee)

            val (savedUser, savedFollowee) = userService.save(foundUser, foundFollowee)
            if (!userService.isFollowing(savedUser, savedFollowee)) {
                ResponseEntity.ok().build()
            } else {
                //TODO: rollback?
                //HTTP Status Code 500: Internal Server Error
                ResponseEntity.status(500).build()
            }
        }, { ResponseEntity.notFound().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/create")
    fun createUser(@RequestBody json: ObjectNode,
                   @RequestHeader("Authorization") token: String,
                   @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        val requiredKeys = arrayOf(ApiKeys.User.USERNAME)
        return if (requiredKeys.all { key -> json.has(key) }) {
            try {
                val user = User(json[ApiKeys.User.USERNAME].asText())

                json.fields().forEach { (key, value) ->
                    when (key) {
                        ApiKeys.User.FULL_NAME -> user.fullName = value.asText()
                        ApiKeys.User.BIOGRAPHY -> user.biography = value.asText()
                    //TODO: add date of birth
                    }
                }

                ResponseEntity.ok(ResponseObjectBuilder.buildUserProfile(userService.save(user)))
            } catch (e: Exception) {
                ResponseEntity.status(500).build<ObjectNode>()
            }
        } else {
            ResponseEntity.badRequest().build()
        }
    }

    @DeleteMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/remove")
    fun removeUser(@PathVariable username: String,
                   @RequestHeader("Authorization") token: String,
                   @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        //TODO: this method only activates the grace periode, user and kweets still need to be removed
        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            foundUser.profileState = User.ProfileState.GRACE
            userService.save(foundUser)
            return ResponseEntity.ok().build()
        }, { ResponseEntity.notFound().build() })
    }

    @PutMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/edit")
    fun editUser(@PathVariable username: String,
                 @RequestBody json: ObjectNode,
                 @RequestHeader("Authorization") token: String,
                 @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            try {
                json.fields().forEach { (key, value) ->
                    when (key) {
                        ApiKeys.User.USERNAME -> foundUser.username = value.asText()
                        ApiKeys.User.FULL_NAME -> foundUser.fullName = value.asText()
                        ApiKeys.User.BIOGRAPHY -> foundUser.biography = value.asText()
                    //TODO: add date of birth
                    }
                }

                userService.save(foundUser)

                ResponseEntity.ok(ResponseObjectBuilder.buildUserProfile(userService.save(foundUser)))
            } catch (e: Exception) {
                ResponseEntity.status(500).build()
            }
        }, { ResponseEntity.notFound().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/like/{kweetId}")
    fun likeKweet(@PathVariable username: String,
                  @PathVariable kweetId: String,
                  @RequestHeader("Authorization") token: String,
                  @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            kweetService.findById(kweetId).ifPresentElse({ foundKweet ->
                ResponseEntity.ok(ResponseObjectBuilder.buildLike(likeService.save(Like(foundUser, foundKweet))))
            }, { ResponseEntity.notFound().build() })
        }, { ResponseEntity.notFound().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/{username}/rekweet/{kweetId}")
    fun rekweet(@PathVariable username: String,
                @PathVariable kweetId: String,
                @RequestHeader("Authorization") token: String,
                @RequestHeader("userId") userId: String
    ): ResponseEntity<ObjectNode> {
        if (!tokenService.validateToken(token, userId)) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()

        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            kweetService.findById(kweetId).ifPresentElse({ foundKweet ->
                ResponseEntity.ok(ResponseObjectBuilder.buildRekweet(rekweetService.save(Rekweet(foundUser, foundKweet))))
            }, { ResponseEntity.notFound().build() })
        }, { ResponseEntity.notFound().build() })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/login")
    fun login(@RequestBody json: ObjectNode): String {
        return userService.findByUsername(json["username"].textValue()).ifPresentElse({ foundUser ->
            tokenService.createToken(foundUser.username, foundUser.userId)
        }, {
            //TODO: handle error more gracefully
            throw NotFoundException("user not found")
        })
    }

    @PostMapping("/${ApiKeys.API_URL_PREFIX}/user/mail/forgot")
    fun sendForgotPasswordMail(@RequestBody json: ObjectNode): Boolean {
        val username = json["username"].asText()

        return userService.findByUsername(username).ifPresentElse({foundUser ->
            httpGetRequest(
                    HttpUrl.parse("http://localhost:1337/mail/forgot" +
                            "/${foundUser.username}" +
                            "/${foundUser.fullName}" +
                            "/${foundUser.emailAddress}")!!, {true},{false})
        }, {
            //TODO: handle this more gracefully
            throw NotFoundException("user not found")
        })
    }
}