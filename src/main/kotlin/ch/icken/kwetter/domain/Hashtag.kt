package ch.icken.kwetter.domain

import ch.icken.kwetter.newId
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import javax.persistence.*

@Entity
@Table(name = "hashtags")
class Hashtag(text: String) {
    @Id
    val hashtagId = newId

    @Column(unique = true)
    val text = text.toLowerCase()

    @ManyToMany(mappedBy = "hashtags")
    val usedBy: MutableSet<Kweet> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Hashtag

        return EqualsBuilder()
                .append(hashtagId, other.hashtagId)
                .append(text, other.text)
                .isEquals
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(hashtagId)
                .append(text)
                .toHashCode()
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }
}