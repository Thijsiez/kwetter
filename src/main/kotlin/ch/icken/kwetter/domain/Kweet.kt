package ch.icken.kwetter.domain

import ch.icken.kwetter.newId
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import javax.persistence.*

@Entity(name = "kweets")
class Kweet(
        user: User,
        content: String,
        var isDeleted: Boolean? = false,

        @OneToOne
        @JoinColumn(name = "reply_to_id")
        val replyTo: Kweet? = null,

        @OneToMany
        @JoinTable(
                name = "quotes",
                joinColumns = [(JoinColumn(name = "quote_id"))],
                inverseJoinColumns = [(JoinColumn(name = "kweet_id"))]
        )
        @OrderColumn(name = "id")
        val quotes: Array<Kweet>? = null,

        @OneToMany
        @JoinTable(
                name = "galleries",
                joinColumns = [(JoinColumn(name = "kweet_id"))],
                inverseJoinColumns = [(JoinColumn(name = "media_id"))]
        )
        @OrderColumn(name = "id")
        val media: Array<Media>? = null
) : Timelineable(user) {
    companion object {
        val mentionRegex = Regex("(?<=\\B@)\\w{1,15}")
        val hashtagRegex = Regex("(?<=\\B#)\\w*[a-zA-Z]+\\w*")
    }

    var content = content
        private set

    val hasMedia = media?.isNotEmpty() == true

    //If this is a "loose" kweet or the origin of many replies, this field will be null
    //If this is a reply, this property will always point to the root "loose" kweet
    @ManyToOne
    @JoinColumn(name = "reply_root_id")
    val replyRoot: Kweet? = replyTo?.replyRoot ?: replyTo

    //If this is the first reply to a kweet, we assign a new thread ID
    //If this is a reply to a reply, we reuse the same thread ID that was passed
    //If this is not a reply but a "loose" kweet, no thread ID is assigned
    val replyThread: String? = if (replyTo != null) replyTo.replyThread ?: newId else null

    @ManyToMany
    @JoinTable(
            name = "mentions",
            joinColumns = [(JoinColumn(name = "kweet_id"))],
            inverseJoinColumns = [(JoinColumn(name = "user_id"))]
    )
    val mentions: MutableSet<User> = HashSet()

    @ManyToMany
    @JoinTable(
            name = "hashtag_usages",
            joinColumns = [(JoinColumn(name = "kweet_id"))],
            inverseJoinColumns = [(JoinColumn(name = "hashtag_id"))]
    )
    val hashtags: MutableSet<Hashtag> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Kweet

        return EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(user, other.user)
                .append(content, other.content)
                .append(replyTo, other.replyTo)
                .append(media, other.media)
                .append(replyRoot, other.replyRoot)
                .append(replyThread, other.replyThread)
                .isEquals
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(user)
                .append(content)
                .append(replyTo)
                .append(media)
                .append(replyRoot)
                .append(replyThread)
                .toHashCode()
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }
}