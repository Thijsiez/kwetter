package ch.icken.kwetter.domain

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity(name = "likes")
class Like(
        user: User,

        @ManyToOne
        @JoinColumn(name = "kweet_id")
        val kweet: Kweet
) : Timelineable(user) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Like

        return EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(user, other.user)
                .append(kweet, other.kweet)
                .isEquals
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(user)
                .append(kweet)
                .toHashCode()
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }
}