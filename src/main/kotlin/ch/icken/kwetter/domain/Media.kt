package ch.icken.kwetter.domain

import ch.icken.kwetter.newId
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import java.io.File
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "media")
class Media(val name:String,
            val url: String) {
    companion object {
        fun encoder(filePath: String): String{
            val bytes = File(filePath).readBytes()
            val base64 = Base64.getEncoder().encodeToString(bytes)
            return base64
        }

        fun decoder(base64Str: String, pathFile: String): Unit{
            val imageByteArray = Base64.getDecoder().decode(base64Str)
            File(pathFile).writeBytes(imageByteArray)
        }
    }

    @Id
    val mediaId = newId

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Media

        return EqualsBuilder()
                .append(mediaId, other.mediaId)
                .append(url, other.url)
                .isEquals
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(mediaId)
                .append(url)
                .toHashCode()
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }
}