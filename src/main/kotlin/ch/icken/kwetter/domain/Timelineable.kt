package ch.icken.kwetter.domain

import ch.icken.kwetter.newId
import ch.icken.kwetter.now
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import org.jetbrains.annotations.TestOnly
import org.springframework.data.annotation.CreatedDate
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.format.annotation.DateTimeFormat.ISO.DATE
import javax.persistence.*

@Entity(name = "timelineables")
@Inheritance(strategy = InheritanceType.JOINED)
abstract class Timelineable(
        @ManyToOne
        @JoinColumn(name = "user_id")
        val user: User
) {
    @Id
    val timelineableId = newId

    @CreatedDate
    @DateTimeFormat(iso = DATE)
    var timestamp = now
        @TestOnly set

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Timelineable

        return EqualsBuilder()
                .append(timelineableId, other.timelineableId)
                .append(timestamp, other.timestamp)
                .isEquals
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(timelineableId)
                .append(timestamp)
                .toHashCode()
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }
}