package ch.icken.kwetter.domain

import ch.icken.kwetter.newId
import ch.icken.kwetter.now
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import org.springframework.data.annotation.CreatedDate
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.format.annotation.DateTimeFormat.ISO.DATE
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(name = "users")
class User(
        username: String,

        var fullName: String? = null,

        var emailAddress: String? = null,

        //TODO implement a better time API
        @DateTimeFormat(iso = DATE)
        var dateOfBirth: Date? = null
) {
    enum class ProfileState {
        ACTIVE, GRACE, INACTIVE
    }

    companion object {
        val usernameRegex = Regex("\\w{1,15}")

        //Checks the username against our predefined username regex pattern
        //If the username matches our pattern, we set return it
        //If the username does not match our pattern, an IllegalArgumentException is thrown
        @Throws(IllegalArgumentException::class)
        fun validateUsername(username: String): String {
            if (!username.matches(usernameRegex))
                throw IllegalArgumentException()
            return username
        }
    }

    @Id
    val userId = newId

    @Column(unique = true)
    var username = validateUsername(username)
        set(value) {
            field = validateUsername(value)
        }

    @CreatedDate
    @DateTimeFormat(iso = DATE)
    val joinDate = now

    var profileState = ProfileState.ACTIVE

    var verified = false

    @OneToOne
    @JoinColumn(name = "profile_pic_id")
    var profilePicture: Media? = null

    var biography: String? = null

    @ManyToMany
    @JoinTable(
            name = "follows",
            joinColumns = [(JoinColumn(name = "follower_id"))],
            inverseJoinColumns = [(JoinColumn(name = "followee_id"))]
    )
    val following: MutableSet<User> = HashSet()

    @ManyToMany(mappedBy = "following")
    val followers: MutableSet<User> = HashSet()

    @ManyToMany(mappedBy = "mentions")
    val mentionedIn: MutableSet<Kweet> = HashSet()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as User

        return EqualsBuilder()
                .append(userId, other.userId)
                .append(username, other.username)
                .append(fullName, other.fullName)
                .append(dateOfBirth, other.dateOfBirth)
                .append(joinDate, other.joinDate)
                .append(profileState, other.profileState)
                .append(verified, other.verified)
                .append(profilePicture, other.profilePicture)
                .append(biography, other.biography)
                .isEquals
    }

    override fun hashCode(): Int {
        return HashCodeBuilder()
                .append(userId)
                .append(username)
                .append(fullName)
                .append(dateOfBirth)
                .append(joinDate)
                .append(profileState)
                .append(verified)
                .append(profilePicture)
                .append(biography)
                .toHashCode()
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }
}