package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.Hashtag
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

internal interface HashtagRepository : JpaRepository<Hashtag, String> {
    fun findByText(text: String): Optional<Hashtag>
}