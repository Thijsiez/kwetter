package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.User
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

internal interface KweetRepository : JpaRepository<Kweet, String> {
    fun findByUserIn(users: Set<User>): List<Kweet>
    fun countByReplyTo(kweet: Kweet): Long
    fun findByReplyTo(kweet: Kweet): List<Kweet>
    fun countByReplyRoot(kweet: Kweet): Long
    fun findByReplyRoot(kweet: Kweet): List<Kweet>
    fun countByReplyThread(thread: String): Long
    fun findByReplyThread(thread: String): List<Kweet>
    fun countByQuotes(kweet: Kweet): Long
    fun findByQuotes(kweet: Kweet): List<Kweet>
    fun findByUserAndReplyRoot(user: User, replyRoot: Kweet?, pageable: Pageable): List<Kweet>
    fun findByUserAndHasMedia(user: User, hadMedia: Boolean, pageable: Pageable): List<Kweet>
}