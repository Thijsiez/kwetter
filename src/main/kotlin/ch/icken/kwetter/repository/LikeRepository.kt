package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.Like
import ch.icken.kwetter.domain.User
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

internal interface LikeRepository : JpaRepository<Like, String> {
    fun findByUser(user: User): List<Like>
    fun findByUser(user: User, pageable: Pageable): List<Like>
    fun findByUserIn(users: Set<User>): List<Like>
    fun countByKweet(kweet: Kweet): Long
}