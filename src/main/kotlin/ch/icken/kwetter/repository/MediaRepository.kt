package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.Media
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

internal interface MediaRepository : JpaRepository<Media, String>{
    fun findByName(name: String): Optional<Media>
}