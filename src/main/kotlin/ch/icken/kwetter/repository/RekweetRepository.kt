package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.Rekweet
import ch.icken.kwetter.domain.User
import org.springframework.data.jpa.repository.JpaRepository

internal interface RekweetRepository : JpaRepository<Rekweet, String> {
    fun findByUser(user: User): List<Rekweet>
    fun findByUserIn(users: Set<User>): List<Rekweet>
    fun countByKweet(kweet: Kweet): Long
}