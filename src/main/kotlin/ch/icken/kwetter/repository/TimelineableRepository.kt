package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.Timelineable
import ch.icken.kwetter.domain.User
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

internal interface TimelineableRepository : JpaRepository<Timelineable, String>{
    fun findByUser(user: User, pageable: Pageable): List<Timelineable>
    fun findByUserIn(users: List<User>, pageable: Pageable): List<Timelineable>
    fun findByUserIn(users: List<User>): List<Timelineable>
}