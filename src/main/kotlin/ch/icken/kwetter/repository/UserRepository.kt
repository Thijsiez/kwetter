package ch.icken.kwetter.repository

import ch.icken.kwetter.domain.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

internal interface UserRepository : JpaRepository<User, String> {
    fun findByUsername(username: String): Optional<User>
    fun findByFollowers_Username(username: String): List<User>
    fun findByFollowing_Username(username: String): List<User>
}