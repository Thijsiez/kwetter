package ch.icken.kwetter.service

import ch.icken.kwetter.domain.Hashtag
import ch.icken.kwetter.repository.HashtagRepository
import ch.icken.kwetter.save
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

interface HashtagService {
    fun deleteAll()

    fun save(hashtag: Hashtag): Hashtag
    fun save(vararg hashtags: Hashtag): List<Hashtag>
    fun saveAll(hashtags: List<Hashtag>): List<Hashtag>

    fun findByText(text: String): Optional<Hashtag>
}

@Suppress("unused")
@Service
internal class HashtagServiceImpl : HashtagService {
    @Autowired
    private lateinit var hashtagRepo: HashtagRepository

    override fun deleteAll() = hashtagRepo.deleteAll()

    override fun save(hashtag: Hashtag) = hashtagRepo.save(hashtag)
    override fun save(vararg hashtags: Hashtag) = hashtagRepo.save(*hashtags)
    override fun saveAll(hashtags: List<Hashtag>): List<Hashtag> = hashtagRepo.saveAll(hashtags)

    override fun findByText(text: String) = hashtagRepo.findByText(text)
}