package ch.icken.kwetter.service

import ch.icken.kwetter.domain.Hashtag
import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.User
import ch.icken.kwetter.filterIsPresent
import ch.icken.kwetter.repository.KweetRepository
import ch.icken.kwetter.save
import ch.icken.kwetter.scopeIfPresent
import ch.icken.kwetter.scopeIfPresentElse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

interface KweetService {
    fun deleteAll()

    fun save(kweet: Kweet): Kweet
    fun save(vararg kweets: Kweet): List<Kweet>
    fun saveAll(kweets: List<Kweet>): List<Kweet>

    fun count(): Long
    fun countByReplyTo(kweet: Kweet): Long
    fun countByReplyRoot(kweet: Kweet): Long
    fun countByReplyThread(thread: String): Long
    fun countByQuotes(kweet: Kweet): Long

    fun findById(kweetId: String): Optional<Kweet>
    fun findAll(): List<Kweet>
    fun findByUserIn(users: Set<User>): List<Kweet>
    fun findByReplyTo(kweet: Kweet): List<Kweet>
    fun findByReplyRoot(kweet: Kweet): List<Kweet>
    fun findByReplyThread(thread: String): List<Kweet>
    fun findByQuotes(kweet: Kweet): List<Kweet>

    fun persistMentionsAndHashtags(kweet: Kweet)

    fun removeKweet(kweet: Kweet): Kweet

//TODO    fun removeById(id: String)
}

@Suppress("unused")
@Service
internal class KweetServiceImpl : KweetService {
    @Autowired
    private lateinit var kweetRepo: KweetRepository

    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var hashtagService: HashtagService

    override fun deleteAll() = kweetRepo.deleteAll()

    override fun save(kweet: Kweet) = kweetRepo.save(kweet)
    override fun save(vararg kweets: Kweet) = kweetRepo.save(*kweets)
    override fun saveAll(kweets: List<Kweet>): List<Kweet> = kweetRepo.saveAll(kweets)

    override fun count() = kweetRepo.count()
    override fun countByReplyTo(kweet: Kweet) = kweetRepo.countByReplyTo(kweet)
    override fun countByReplyRoot(kweet: Kweet) = kweetRepo.countByReplyRoot(kweet)
    override fun countByReplyThread(thread: String) = kweetRepo.countByReplyThread(thread)
    override fun countByQuotes(kweet: Kweet) = kweetRepo.countByQuotes(kweet)

    override fun findById(kweetId: String) = kweetRepo.findById(kweetId)
    override fun findAll(): List<Kweet> = kweetRepo.findAll()
    override fun findByUserIn(users: Set<User>) = kweetRepo.findByUserIn(users)
    override fun findByReplyTo(kweet: Kweet) = kweetRepo.findByReplyTo(kweet)
    override fun findByReplyRoot(kweet: Kweet) = kweetRepo.findByReplyRoot(kweet)
    override fun findByReplyThread(thread: String) = kweetRepo.findByReplyThread(thread)
    override fun findByQuotes(kweet: Kweet) = kweetRepo.findByQuotes(kweet)

    override fun persistMentionsAndHashtags(kweet: Kweet) {
        userService.saveAll(Kweet.mentionRegex.findAll(kweet.content).map { mentionMatch ->
            userService.findByUsername(mentionMatch.value).scopeIfPresent { matchedUser ->
                kweet.mentions.add(matchedUser)
                matchedUser.mentionedIn.add(kweet)
            }
        }.filterIsPresent().toList())
        hashtagService.saveAll(Kweet.hashtagRegex.findAll(kweet.content).map { hashtagMatch ->
            val hashtagText = hashtagMatch.value
            hashtagService.findByText(hashtagText).scopeIfPresentElse({ foundHashtag ->
                kweet.hashtags.add(foundHashtag)
                foundHashtag.usedBy.add(kweet)
            }, {
                val newHashtag = Hashtag(hashtagText)
                kweet.hashtags.add(newHashtag)
                newHashtag.usedBy.add(kweet)
                newHashtag
            })
        }.toList())
    }

    override fun removeKweet(kweet: Kweet): Kweet {
        kweet.isDeleted = true
        return kweetRepo.save(kweet)
    }

    //TODO
//    override fun removeById(id: String) {
//        kweetRepo.removeById(id)
//    }
}