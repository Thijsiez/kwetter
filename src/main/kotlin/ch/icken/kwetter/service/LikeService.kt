package ch.icken.kwetter.service

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.Like
import ch.icken.kwetter.domain.User
import ch.icken.kwetter.repository.LikeRepository
import ch.icken.kwetter.save
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

interface LikeService {
    fun deleteAll()

    fun save(like: Like): Like
    fun save(vararg likes: Like): List<Like>
    fun saveAll(likes: List<Like>): List<Like>

    fun count(): Long
    fun countByKweet(kweet: Kweet): Long

    fun findById(likeId: String): Optional<Like>
    fun findByUserIn(users: Set<User>): List<Like>
    fun findByUser(user: User): List<Like>
}

@Suppress("unused")
@Service
internal class LikeServiceImpl : LikeService {
    @Autowired
    private lateinit var likeRepo: LikeRepository

    override fun deleteAll() = likeRepo.deleteAll()

    override fun save(like: Like) = likeRepo.save(like)
    override fun save(vararg likes: Like) = likeRepo.save(*likes)
    override fun saveAll(likes: List<Like>): List<Like> = likeRepo.saveAll(likes)

    override fun count() = likeRepo.count()
    override fun countByKweet(kweet: Kweet) = likeRepo.countByKweet(kweet)

    override fun findById(likeId: String) = likeRepo.findById(likeId)
    override fun findByUserIn(users: Set<User>) = likeRepo.findByUserIn(users)
    override fun findByUser(user: User) = likeRepo.findByUser(user)
}