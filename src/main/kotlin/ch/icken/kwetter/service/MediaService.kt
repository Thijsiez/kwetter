package ch.icken.kwetter.service

import ch.icken.kwetter.domain.Media
import ch.icken.kwetter.repository.MediaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.util.*

interface MediaService {
    fun deleteAll()

    fun save(media: Media): Media

    fun storeImage(file: MultipartFile): Media

    fun findById(mediaId: String): Optional<Media>
}

@Suppress("unused")
@Service
internal class MediaServiceImpl : MediaService {
    @Autowired
    private lateinit var mediaRepo: MediaRepository

    override fun deleteAll() = mediaRepo.deleteAll()

    override fun save(media: Media) = mediaRepo.save(media)

    override fun storeImage(file: MultipartFile) = Media(file.originalFilename!!, file.let {
        val convFile = File(it.originalFilename)
        convFile.createNewFile()
        FileOutputStream(convFile).use {
            it.write(file.bytes)
        }
        convFile.canonicalPath
    })

    override fun findById(mediaId: String) = mediaRepo.findById(mediaId)
}