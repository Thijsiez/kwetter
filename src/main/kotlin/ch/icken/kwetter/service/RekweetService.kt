package ch.icken.kwetter.service

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.Rekweet
import ch.icken.kwetter.domain.User
import ch.icken.kwetter.repository.RekweetRepository
import ch.icken.kwetter.save
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

interface RekweetService {
    fun deleteAll()

    fun save(rekweet: Rekweet): Rekweet
    fun save(vararg rekweets: Rekweet): List<Rekweet>
    fun saveAll(rekweets: List<Rekweet>): List<Rekweet>

    fun count(): Long
    fun countByKweet(kweet: Kweet): Long

    fun findById(rekweetId: String): Optional<Rekweet>
    fun findByUserIn(users: Set<User>): List<Rekweet>
    fun findByUser(user: User): List<Rekweet>
}

@Suppress("unused")
@Service
internal class RekweetServiceImpl : RekweetService {
    @Autowired
    private lateinit var rekweetRepo: RekweetRepository

    override fun deleteAll() = rekweetRepo.deleteAll()

    override fun save(rekweet: Rekweet) = rekweetRepo.save(rekweet)
    override fun save(vararg rekweets: Rekweet) = rekweetRepo.save(*rekweets)
    override fun saveAll(rekweets: List<Rekweet>): List<Rekweet> = rekweetRepo.saveAll(rekweets)

    override fun count() = rekweetRepo.count()
    override fun countByKweet(kweet: Kweet) = rekweetRepo.countByKweet(kweet)

    override fun findById(rekweetId: String) = rekweetRepo.findById(rekweetId)
    override fun findByUserIn(users: Set<User>) = rekweetRepo.findByUserIn(users)
    override fun findByUser(user: User) = rekweetRepo.findByUser(user)
}