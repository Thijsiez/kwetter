package ch.icken.kwetter.service

import ch.icken.kwetter.repository.TimelineableRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface TimelineableService {

}

@Suppress("unused")
@Service
internal class TimelineableServiceImpl : TimelineableService {
    @Autowired
    private lateinit var timelineableRepo: TimelineableRepository
}