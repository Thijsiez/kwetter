package ch.icken.kwetter.service

import ch.icken.kwetter.httpGetRequest
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.HttpUrl
import okhttp3.RequestBody
import org.springframework.stereotype.Service
import okhttp3.FormBody



interface TokenService {
    fun validateToken(token: String, userId: String): Boolean
    fun createToken(username: String, userId: String): String
}

@Suppress("unused")
@Service
internal class TokenServiceImpl : TokenService {
    companion object {
        private val jsonMapper = ObjectMapper()
    }

    override fun validateToken(token: String, userId: String): Boolean {
        return httpGetRequest(
                HttpUrl.parse("http://localhost:6969/security/token/$token/$userId/valid")!!,
                { response ->
                    val jsonResponse = jsonMapper.readTree(response)
                    val result = jsonResponse["validity"].textValue()
                    result == "true"
                },
                { response ->
                    //TODO: handle error more gracefully
                    //throw NotFoundException("token couldn't be retrieved $response")
                    false
                })
    }

    override fun createToken(username: String, userId: String): String {
        return httpGetRequest(
                HttpUrl.parse("http://localhost:6969/security/token/$username/$userId")!!,
                { response ->
                    val jsonResponse = jsonMapper.readTree(response)
                    jsonResponse["token"].textValue()
                },
                { response ->
                    //TODO: handle error more gracefully
                    throw Exception("token couldn't be generated $response")
                })
    }
}
