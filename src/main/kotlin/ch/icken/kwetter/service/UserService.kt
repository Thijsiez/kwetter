package ch.icken.kwetter.service

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.Timelineable
import ch.icken.kwetter.domain.User
import ch.icken.kwetter.findByIds
import ch.icken.kwetter.repository.KweetRepository
import ch.icken.kwetter.repository.LikeRepository
import ch.icken.kwetter.repository.TimelineableRepository
import ch.icken.kwetter.repository.UserRepository
import ch.icken.kwetter.save
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*

interface UserService {
    fun deleteAll()

    fun save(user: User): User
    fun save(vararg users: User): List<User>
    fun saveAll(users: List<User>): List<User>

    fun count(): Long

    fun findById(userId: String): Optional<User>
    fun findByIds(vararg userIds: String): List<Optional<User>>
    fun findAll(): List<User>
    fun findByUsername(username: String): Optional<User>
    fun findByUsernames(vararg usernames: String): List<Optional<User>>
    fun findFollows(username: String): List<User>
    fun findFollowers(username: String): List<User>

    fun startFollowing(follower: User, followee: User)
    fun stopFollowing(follower: User, followee: User)
    fun isFollowing(follower: User, followee: User): Boolean

    fun getKweets(user: User, page: Int): List<Timelineable>
    fun getKweetsAndReplies(user: User, page: Int): List<Timelineable>
    fun getKweetsWithMedia(user: User, page: Int): List<Timelineable>
    fun getTimeline(user: User, page: Int): List<Timelineable>
    fun getTimelineSearch(user: User, search: String): List<Timelineable>
    fun getLikes(user: User, page: Int): List<Timelineable>
}

@Suppress("unused")
@Service
internal class UserServiceImpl : UserService {
    companion object {
        const val PageSize = 5 //TODO: make realistic page size
    }

    @Autowired
    private lateinit var userRepo: UserRepository
    @Autowired
    private lateinit var timelineableRepo: TimelineableRepository
    @Autowired
    private lateinit var kweetRepo: KweetRepository
    @Autowired
    private lateinit var likeRepo: LikeRepository

    override fun deleteAll() = userRepo.deleteAll()

    override fun save(user: User) = userRepo.save(user)
    override fun save(vararg users: User) = userRepo.save(*users)
    override fun saveAll(users: List<User>): List<User> = userRepo.saveAll(users)

    override fun count() = userRepo.count()

    override fun findById(userId: String) = userRepo.findById(userId)
    override fun findByIds(vararg userIds: String) = userRepo.findByIds(*userIds)
    override fun findAll(): List<User> = userRepo.findAll()
    override fun findByUsername(username: String) = userRepo.findByUsername(username)
    override fun findByUsernames(vararg usernames: String) = usernames.map { findByUsername(it) }
    override fun findFollows(username: String) = userRepo.findByFollowers_Username(username)
    override fun findFollowers(username: String) = userRepo.findByFollowing_Username(username)

    override fun startFollowing(follower: User, followee: User) {
        follower.following.add(followee)
        followee.followers.add(follower)
    }

    override fun stopFollowing(follower: User, followee: User) {
        follower.following.remove(followee)
        followee.followers.remove(follower)
    }

    override fun isFollowing(follower: User, followee: User) =
            follower.following.contains(followee) && followee.followers.contains(follower)

    override fun getKweets(user: User, page: Int): List<Kweet> {
        val sort = Sort(Sort.Direction.DESC, "timestamp")
        return kweetRepo.findByUserAndReplyRoot(user, null, PageRequest.of(page, PageSize, sort))
    }

    override fun getKweetsAndReplies(user: User, page: Int): List<Timelineable> {
        val sort = Sort(Sort.Direction.DESC, "timestamp")
        return timelineableRepo.findByUser(user, PageRequest.of(page, PageSize, sort))
    }

    override fun getKweetsWithMedia(user: User, page: Int): List<Kweet> {
        val sort = Sort(Sort.Direction.DESC, "timestamp")
        return kweetRepo.findByUserAndHasMedia(user, true, PageRequest.of(page, PageSize, sort))
    }

    override fun getTimeline(user: User, page: Int): List<Timelineable> {
        val sort = Sort(Sort.Direction.DESC, "timestamp")
        return timelineableRepo.findByUserIn(user.following.toList(), PageRequest.of(page, PageSize, sort))
    }

    override fun getTimelineSearch(user: User, search: String): List<Timelineable> {
        return timelineableRepo.findByUserIn(user.following.toList())
    }

    override fun getLikes(user: User, page: Int): List<Timelineable> {
        val sort = Sort(Sort.Direction.DESC, "timestamp")
        return likeRepo.findByUser(user, PageRequest.of(page, PageSize, sort))
    }
}