package ch.icken.kwetter.session

import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Component
class SessionAuthProvider : AuthenticationProvider {
    @Autowired
    private lateinit var userService: UserService

    @Throws(BadCredentialsException::class)
    override fun authenticate(authentication: Authentication): Authentication {
        return userService.findByUsername(authentication.name).ifPresentElse({ foundUser ->
            UsernamePasswordAuthenticationToken(foundUser.username, null, emptyList())
        }, { throw BadCredentialsException("User does not exist") })
    }

    override fun supports(authentication: Class<*>) =
            authentication == UsernamePasswordAuthenticationToken::class.java
}