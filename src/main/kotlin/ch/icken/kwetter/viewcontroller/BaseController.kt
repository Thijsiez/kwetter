package ch.icken.kwetter.viewcontroller

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder

abstract class BaseController {
    companion object {
        const val ThisPathKey = "thisPath"
    }

    protected val auth: Authentication get() =
        SecurityContextHolder.getContext().authentication
}