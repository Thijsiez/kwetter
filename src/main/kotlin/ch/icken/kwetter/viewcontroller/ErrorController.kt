package ch.icken.kwetter.viewcontroller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class ErrorController {
    @GetMapping("/oops")
    fun getNotFound() = "error/404"
}