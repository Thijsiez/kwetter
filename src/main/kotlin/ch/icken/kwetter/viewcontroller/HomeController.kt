package ch.icken.kwetter.viewcontroller

import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class HomeController : BaseController() {
    companion object {
        private const val HomeThymeleafTemplateKey = "timeline"
        private const val WhenNotFoundRedirect = "redirect:/oops"
        private const val UserCardKey = "userCard"
        private const val TimelineablesKey = "timelineables"

        private const val LoginThymeleafTemplateKey = "login"
        private const val LoginErrorFlagKey = "loginError"
    }

    @Autowired
    private lateinit var userService: UserService

    @GetMapping("/")
    fun getHomepage(model: Model) =
            userService.findByUsername(auth.name).ifPresentElse({ foundUser ->
                model.addAttribute(ThisPathKey, "/")
                model.addAttribute(UserCardKey, UserCardModel(foundUser))
                model.addAttribute(TimelineablesKey,
                        userService.getTimeline(foundUser, 0).map { TimelineableModel.map(it) })

                HomeThymeleafTemplateKey
            }, { WhenNotFoundRedirect })

    @GetMapping("/login")
    fun getLogin() = LoginThymeleafTemplateKey

    @GetMapping("/login/error")
    fun getLoginError(model: Model): String {
        model.addAttribute(LoginErrorFlagKey, true)
        return LoginThymeleafTemplateKey
    }
}