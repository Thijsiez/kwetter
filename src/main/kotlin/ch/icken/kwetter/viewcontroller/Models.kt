package ch.icken.kwetter.viewcontroller

import ch.icken.kwetter.domain.*
import ch.icken.kwetter.prettyTime

/*
These are all POKO's (Plain Old Kotlin Objects) to map to
They may not be necessary but they streamline the templating process
 */

//Default values
const val DefaultProfilePictureUrl = "https://bulma.io/images/placeholders/480x480.png"

data class UserCardModel(
        val username: String,
        val fullName: String? = null,
        val profilePicture: String = DefaultProfilePictureUrl,
        val amountOfKweets: Int,
        val amountOfFollowing: Int,
        val amountOfFollowers: Int
) {
    constructor(user: User) : this(
            username = user.username,
            fullName = user.fullName,
            profilePicture = user.profilePicture?.url ?: DefaultProfilePictureUrl,
            //TODO fill with actual data
            amountOfKweets = 13,
            amountOfFollowing = 3,
            amountOfFollowers = 7
    )
}
data class UserProfileModel(
        val username: String,
        val fullName: String? = null,
        val profilePicture: String = DefaultProfilePictureUrl,
        val joinDate: String,
        val biography: String? = null,
        val amountOfKweets: Int,
        val amountOfFollowing: Int,
        val amountOfFollowers: Int,
        val amountOfLikes: Int
) {
    constructor(user: User) : this(
            username = user.username,
            fullName = user.fullName,
            profilePicture = user.profilePicture?.url ?: DefaultProfilePictureUrl,
            joinDate = prettyTime.format(user.joinDate),
            biography = user.biography,
            //TODO fill with actual data
            amountOfKweets = 1337,
            amountOfFollowing = 69,
            amountOfFollowers = 42,
            amountOfLikes = 420
    )
}
data class FollowCardModel(
        val username: String,
        val fullName: String? = null,
        val profilePicture: String = DefaultProfilePictureUrl,
        val biography: String? = null
) {
    constructor(user: User) : this(
            username = user.username,
            fullName = user.fullName,
            profilePicture = user.profilePicture?.url ?: DefaultProfilePictureUrl,
            biography = user.biography
    )
}
data class TimelineableModel(
        val id: String,
        val username: String,
        val fullName: String? = null,
        val profilePicture: String = DefaultProfilePictureUrl,
        val timestamp: String,
        val content: String,
        val amountOfReplies: Int,
        val amountOfRekweets: Int,
        val amountOfLikes: Int,
        val rekweeted: Boolean = false,
        val liked: Boolean = false,
        val rekweetingOrLikingUsername: String? = null,
        val rekweetingOrLikingName: String? = null,
        val hasMedia: Boolean = false,
        //TODO send more than just the first picture
        val media: String? = null,
        val hasQuotes: Boolean = false,
        val quotes: List<QuoteModel>? = null
) {
    companion object {
        fun map(timelineable: Timelineable) = when (timelineable) {
            is Kweet -> TimelineableModel(timelineable)
            is Rekweet -> TimelineableModel(timelineable)
            is Like -> TimelineableModel(timelineable)
            else -> throw IllegalStateException("This is impossible")
        }
    }

    constructor(kweet: Kweet) : this(
            id = kweet.timelineableId,
            username = kweet.user.username,
            fullName = kweet.user.fullName,
            profilePicture = kweet.user.profilePicture?.url ?: DefaultProfilePictureUrl,
            timestamp = prettyTime.format(kweet.timestamp),
            content = kweet.content,
            //TODO fill with actual data
            amountOfReplies = 13,
            amountOfRekweets = 3,
            amountOfLikes = 7,
            hasMedia = kweet.media?.isNotEmpty() == true,
            media = kweet.media?.firstOrNull()?.url,
            hasQuotes = kweet.quotes?.isNotEmpty() == true,
            quotes = kweet.quotes?.map { QuoteModel(it) }
    )
    constructor(rekweet: Rekweet) : this(
            id = rekweet.timelineableId,
            username = rekweet.kweet.user.username,
            fullName = rekweet.kweet.user.fullName,
            profilePicture = rekweet.kweet.user.profilePicture?.url ?: DefaultProfilePictureUrl,
            timestamp = prettyTime.format(rekweet.kweet.timestamp),
            content = rekweet.kweet.content,
            //TODO fill with actual data
            amountOfReplies = 13,
            amountOfRekweets = 3,
            amountOfLikes = 7,
            rekweeted = true,
            rekweetingOrLikingName = rekweet.user.fullName ?: rekweet.user.username,
            hasMedia = rekweet.kweet.media?.isNotEmpty() == true,
            media = rekweet.kweet.media?.firstOrNull()?.url,
            hasQuotes = rekweet.kweet.quotes?.isNotEmpty() == true,
            quotes = rekweet.kweet.quotes?.map { QuoteModel(it) }
    )
    constructor(like: Like) : this(
            id = like.timelineableId,
            username = like.kweet.user.username,
            fullName = like.kweet.user.fullName,
            profilePicture = like.kweet.user.profilePicture?.url ?: DefaultProfilePictureUrl,
            timestamp = prettyTime.format(like.kweet.timestamp),
            content = like.kweet.content,
            //TODO fill with actual data
            amountOfReplies = 13,
            amountOfRekweets = 3,
            amountOfLikes = 7,
            liked = true,
            rekweetingOrLikingName = like.user.fullName ?: like.user.username,
            hasMedia = like.kweet.media?.isNotEmpty() == true,
            media = like.kweet.media?.firstOrNull()?.url,
            hasQuotes = like.kweet.quotes?.isNotEmpty() == true,
            quotes = like.kweet.quotes?.map { QuoteModel(it) }
    )
}
data class QuoteModel(
        val id: String,
        val username: String,
        val fullName: String? = null,
        val timestamp: String,
        val content: String
) {
    constructor(kweet: Kweet) : this(
            id = kweet.timelineableId,
            username = kweet.user.username,
            fullName = kweet.user.fullName,
            timestamp = prettyTime.format(kweet.timestamp),
            content = kweet.content
    )
}