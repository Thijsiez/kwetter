package ch.icken.kwetter.viewcontroller

import ch.icken.kwetter.domain.User
import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.service.LikeService
import ch.icken.kwetter.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class ProfileController : BaseController() {
    companion object {
        private const val ThymeleafTemplateKey = "profile"
        private const val WhenNotFoundRedirect = "redirect:/oops"
        private const val UserProfileKey = "userProfile"
        private const val TimelineablesKey = "timelineables"
        private const val FollowCardsKey = "followCards"

        private const val SubPageKey = "subPage"
        //SubPage Suffixes
        private const val AndRepliesSuffix = "and_replies"
        private const val WithMediaSuffix = "with_media"
        private const val FollowingSuffix = "following"
        private const val FollowersSuffix = "followers"
        private const val LikesSuffix = "likes"
    }

    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var likeService: LikeService

    @GetMapping("/{username}")
    fun getProfile(@PathVariable username: String, model: Model) =
            findUserAndFillModel(username) { foundUser ->
                model.addAttribute(ThisPathKey, "/$username")
                model.addAttribute(UserProfileKey, UserProfileModel(foundUser))
                model.addAttribute(TimelineablesKey,
                        userService.getKweets(foundUser, 0).map { TimelineableModel.map(it) })
            }

    @GetMapping("/{username}/$AndRepliesSuffix")
    fun getProfileAndReplies(@PathVariable username: String, model: Model) =
            findUserAndFillModel(username) { foundUser ->
                model.addAttribute(ThisPathKey, "/$username/$AndRepliesSuffix")
                model.addAttribute(SubPageKey, AndRepliesSuffix)
                model.addAttribute(UserProfileKey, UserProfileModel(foundUser))
                model.addAttribute(TimelineablesKey,
                        userService.getKweetsAndReplies(foundUser, 0).map { TimelineableModel.map(it) })
            }

    @GetMapping("/{username}/$WithMediaSuffix")
    fun getProfileWithMedia(@PathVariable username: String, model: Model) =
            findUserAndFillModel(username) { foundUser ->
                model.addAttribute(ThisPathKey, "/$username/$WithMediaSuffix")
                model.addAttribute(SubPageKey, WithMediaSuffix)
                model.addAttribute(UserProfileKey, UserProfileModel(foundUser))
                model.addAttribute(TimelineablesKey,
                        userService.getKweetsWithMedia(foundUser, 0).map { TimelineableModel.map(it) })
            }

    @GetMapping("/{username}/$FollowingSuffix")
    fun getProfileFollowing(@PathVariable username: String, model: Model) =
            findUserAndFillModel(username) { foundUser ->
                model.addAttribute(SubPageKey, FollowingSuffix)
                model.addAttribute(UserProfileKey, UserProfileModel(foundUser))
                model.addAttribute(FollowCardsKey, userService.findFollows(username).map { FollowCardModel(it) })
            }

    @GetMapping("/{username}/$FollowersSuffix")
    fun getProfileFollowers(@PathVariable username: String, model: Model) =
            findUserAndFillModel(username) { foundUser ->
                model.addAttribute(SubPageKey, FollowersSuffix)
                model.addAttribute(UserProfileKey, UserProfileModel(foundUser))
                model.addAttribute(FollowCardsKey, userService.findFollowers(username).map { FollowCardModel(it) })
            }

    @PostMapping("/{follower}/follow/{followee}")
    fun postFollowUser(@PathVariable follower: String, @PathVariable followee: String,
                       @RequestParam redirect: String): String {
        return userService.findByUsername(follower).ifPresentElse({ foundFollower ->
            userService.findByUsername(followee).ifPresentElse({ foundFollowee ->
                userService.startFollowing(foundFollower, foundFollowee)
                userService.save(foundFollower, foundFollowee)
                "redirect:$redirect"
            }, { WhenNotFoundRedirect })
        }, { WhenNotFoundRedirect })
    }

    @PostMapping("/{follower}/unfollow/{followee}")
    fun postUnfollowUser(@PathVariable follower: String, @PathVariable followee: String,
                         @RequestParam redirect: String): String {
        return userService.findByUsername(follower).ifPresentElse({ foundFollower ->
            userService.findByUsername(followee).ifPresentElse({ foundFollowee ->
                userService.stopFollowing(foundFollower, foundFollowee)
                userService.save(foundFollower, foundFollowee)
                "redirect:$redirect"
            }, { WhenNotFoundRedirect })
        }, { WhenNotFoundRedirect })
    }

    @GetMapping("/{username}/$LikesSuffix")
    fun getProfileLikes(@PathVariable username: String, model: Model) =
            findUserAndFillModel(username) { foundUser ->
                model.addAttribute(ThisPathKey, "/$username/$LikesSuffix")
                model.addAttribute(SubPageKey, LikesSuffix)
                model.addAttribute(UserProfileKey, UserProfileModel(foundUser))
                model.addAttribute(TimelineablesKey,
                        likeService.findByUser(foundUser).map { TimelineableModel(it) })
            }

    private inline fun findUserAndFillModel(username: String, fillModel: (foundUser: User) -> Unit): String {
        return userService.findByUsername(username).ifPresentElse({
            fillModel(it)
            ThymeleafTemplateKey
        }, {
            WhenNotFoundRedirect
        })
    }
}