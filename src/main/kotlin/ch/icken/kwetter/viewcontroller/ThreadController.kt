package ch.icken.kwetter.viewcontroller

import ch.icken.kwetter.domain.Kweet
import ch.icken.kwetter.domain.Like
import ch.icken.kwetter.domain.Rekweet
import ch.icken.kwetter.ifPresentElse
import ch.icken.kwetter.service.KweetService
import ch.icken.kwetter.service.LikeService
import ch.icken.kwetter.service.RekweetService
import ch.icken.kwetter.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
class ThreadController : BaseController() {
    data class NewKweetObject(
            val replyToKweetId: String? = null,
            val content: String = ""
    )

    companion object {
        private const val ThymeleafTemplateKey = "kweet"
        private const val WhenNotFoundRedirect = "redirect:/oops"
        private const val ReplyRootKey = "replyRoot"
        private const val NewKweetObjectKey = "newKweetObject"
        private const val RepliesKey = "replies"

        private const val PageTypeKey = "pageType"
        //Page Types
        private const val KweetAndRepliesType = "kweet"
        private const val NewKweetType = "newKweet"
        private const val ReplyToKweetType = "replyTo"
    }

    @Autowired
    private lateinit var userService: UserService
    @Autowired
    private lateinit var kweetService: KweetService
    @Autowired
    private lateinit var rekweetService: RekweetService
    @Autowired
    private lateinit var likeService: LikeService

    @GetMapping("/kweet/{kweetId}")
    fun getKweet(@PathVariable kweetId: String, model: Model) =
            findKweetAndFillModel(kweetId) { foundKweet ->
                model.addAttribute(ThisPathKey, "/kweet/$kweetId")
                model.addAttribute(PageTypeKey, KweetAndRepliesType)
                model.addAttribute(ReplyRootKey, TimelineableModel(foundKweet))
                model.addAttribute(RepliesKey,
                        kweetService.findByReplyRoot(foundKweet).map { TimelineableModel(it) })
            }

    @GetMapping("/kweet/new")
    fun getNewKweet(model: Model): String {
        model.addAttribute(PageTypeKey, NewKweetType)
        model.addAttribute(NewKweetObjectKey, NewKweetObject())
        return ThymeleafTemplateKey
    }

    @PostMapping("/kweet/new")
    fun postNewKweet(@ModelAttribute newKweetObject: NewKweetObject): String {
        return userService.findByUsername(auth.name).ifPresentElse({ foundUser ->
            val savedKweet = kweetService.save(Kweet(foundUser, newKweetObject.content))
            //TODO persist mentions and hashtags
            "redirect:/kweet/${savedKweet.timelineableId}"
        }, { WhenNotFoundRedirect })
    }

    @PostMapping("/user/{username}/rekweet/{kweetId}")
    fun postNewRekweet(@PathVariable username: String, @PathVariable kweetId: String,
                       @RequestParam redirect: String): String {
        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            kweetService.findById(kweetId).ifPresentElse({ foundKweet ->
                rekweetService.save(Rekweet(foundUser, foundKweet))
                "redirect:$redirect"
            }, { WhenNotFoundRedirect })
        }, { WhenNotFoundRedirect })
    }

    @PostMapping("/user/{username}/like/{kweetId}")
    fun postNewLike(@PathVariable username: String, @PathVariable kweetId: String,
                    @RequestParam redirect: String): String {
        return userService.findByUsername(username).ifPresentElse({ foundUser ->
            kweetService.findById(kweetId).ifPresentElse({ foundKweet ->
                likeService.save(Like(foundUser, foundKweet))
                "redirect:$redirect"
            }, { WhenNotFoundRedirect })
        }, { WhenNotFoundRedirect })
    }

    @GetMapping("/kweet/{kweetId}/reply")
    fun getKweetAndReply(@PathVariable kweetId: String, model: Model) =
            findKweetAndFillModel(kweetId) { foundKweet ->
                model.addAttribute(ThisPathKey, "/kweet/$kweetId/reply")
                model.addAttribute(PageTypeKey, ReplyToKweetType)
                model.addAttribute(ReplyRootKey, TimelineableModel(foundKweet))
                model.addAttribute(RepliesKey,
                        kweetService.findByReplyRoot(foundKweet).map { TimelineableModel(it) })
            }

    private inline fun findKweetAndFillModel(kweetId: String, fillModel: (foundKweet: Kweet) -> Unit): String {
        return kweetService.findById(kweetId).ifPresentElse({ foundKweet ->
            fillModel(foundKweet.replyRoot ?: foundKweet)
            ThymeleafTemplateKey
        }, {
            WhenNotFoundRedirect
        })
    }
}