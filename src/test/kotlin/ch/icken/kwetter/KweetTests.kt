package ch.icken.kwetter

import ch.icken.kwetter.domain.*
import ch.icken.kwetter.service.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@RunWith(SpringRunner::class)
@SpringBootTest
class KweetTests {
    companion object TestData {
        internal val doNotUseThisThijs = UserTests.doNotUseThisThijs
        internal val doNotUseThisDennis = UserTests.doNotUseThisDennis
        internal val doNotUseThisSjors = User("RapperSjors")
    }

    //region Repositories
    @Autowired private lateinit var userService: UserService
    @Autowired private lateinit var kweetService: KweetService
    @Autowired private lateinit var rekweetService: RekweetService
    @Autowired private lateinit var likeService: LikeService
    @Autowired private lateinit var mediaService: MediaService
    @Autowired private lateinit var hashtagService: HashtagService
    //endregion

    private lateinit var thijs: User
    private lateinit var dennis: User
    private lateinit var sjors: User

    private lateinit var testKweet: Kweet

    @Before
    @Transactional
    fun insertTestData() {
        userService.deleteAll()

        thijs = userService.save(doNotUseThisThijs)
        dennis = userService.save(doNotUseThisDennis)
        sjors = userService.save(doNotUseThisSjors)

        kweetService.deleteAll()

        kweetService.save(
                Kweet(thijs, "Hello, World!"),
                Kweet(dennis, "You just lost the game :P"),
                Kweet(sjors, "Yeah, let's go!!"),
                Kweet(thijs, "Kotlin for life baby <3"),
                Kweet(dennis, "Ik wil echt die nieuwe MacBook Pro, man")
        )
        testKweet = kweetService.save(Kweet(sjors, "Ja sorry, ik heb net pindakaas gegeten"))

        rekweetService.deleteAll()
        likeService.deleteAll()
        hashtagService.deleteAll()
    }

    @Test
    @Transactional
    fun countKweets() {
        assertEquals(6L, kweetService.count())
        kweetService.findAll().forEach { println(it) }
    }

    @Test
    @Transactional
    fun replyTo() {
        kweetService.save(Kweet(thijs, "Gatverdamme Sjors :(", testKweet))

        assertEquals(1L, kweetService.countByReplyTo(testKweet))
        kweetService.findByReplyTo(testKweet).firstOrNull().scopeIfNotNullElseFail("Reply not found") { kweet ->
            assertEquals(testKweet, kweet.replyTo, "Replied-to kweet does not match")
        }
    }

    @Test
    @Transactional
    fun quote() {
        kweetService.save(Kweet(dennis, "Daar zou ik dood aan gaan...", quotes = arrayOf(testKweet)))

        assertEquals(1L, kweetService.countByQuotes(testKweet))
        kweetService.findByQuotes(testKweet).firstOrNull().scopeIfNotNullElseFail("Quotation not found") { kweet ->
            assertEquals(testKweet, kweet.quotes?.firstOrNull(), "Quoted kweet does not match")
        }
    }

    @Test
    @Transactional
    fun replyThread() {
        val firstReply = kweetService.save(Kweet(dennis, "PEANUT BUTTER JEEEELLLYYY", testKweet))
        val secondReply = kweetService.save(Kweet(thijs, "Nee, dat is ook ranzig man!", firstReply))
        //Add a tweet in between to make sure the query is retrieving the correct kweets
        kweetService.save(Kweet(sjors, "Snickers, geef geld")) //replyTo and replyThread not specified
        kweetService.save(Kweet(dennis, "Niet zo janken Thissa", secondReply))

        assertEquals(3L, kweetService.countByReplyRoot(testKweet))
        kweetService.findByReplyRoot(testKweet).scopeForEach { kweet ->
            assertEquals(testKweet, kweet.replyRoot, "Reply root kweet does not match")
        }

        assertEquals(3L, kweetService.countByReplyThread(firstReply.replyThread ?: ""))
        kweetService.findByReplyThread(firstReply.replyThread ?: "").scopeForEach { kweet ->
            assertEquals(firstReply.replyThread, kweet.replyThread, "Reply thread ID does not match")
        }
    }

    @Test
    @Transactional
    fun media() {
        val testMedia = mediaService.save(Media("image", "https://pbs.twimg.com/profile_images/887324211315789824/ah1EG6u3.jpg"))
        val mediaKweet = Kweet(sjors, "Snickers voor het leven! Yeah, lets go!", media = arrayOf(testMedia))
        kweetService.save(mediaKweet)

        kweetService.findById(mediaKweet.timelineableId).scopeIfPresentElseFail("Kweet not found") { kweet ->
            assertEquals(testMedia, kweet.media?.firstOrNull(), "Media does not match")
        }
    }

    @Test
    @Transactional
    @Ignore
    fun mentionsAndHashtags() {
        //Save the user @SnickersNL for this test so we can mention them
        val snickers = User("SnickersNL", "Snickers Nederland")
        val savedSnickers = userService.save(snickers)

        val kweet = Kweet(sjors, "Ik houd van @snickersnl! #hardgaan")
        kweetService.persistMentionsAndHashtags(kweet)
        val savedKweet = kweetService.save(kweet)

        //Hashtags are parsed, not passed so we make this object just for comparisons
        val targetHashtag = Hashtag("hardgaan")

        ////////
        // Separate the local objects from the objects we can pull from the database
        ////////

        val foundKweet = kweetService.findById(kweet.timelineableId)
        assertTrue(foundKweet.isPresent, "Kweet not found")
        assertEquals(savedKweet, foundKweet.get(), "Persisted kweet does not match")

        val foundSnickers = userService.findByUsername(snickers.username)
        assertTrue(foundSnickers.isPresent, "User not found")
        assertEquals(savedSnickers, foundSnickers.get(), "Persisted user does not match")

        val foundHashtag = hashtagService.findByText(targetHashtag.text)
        assertTrue(foundHashtag.isPresent, "Hashtag not found")
        assertEquals(targetHashtag, foundHashtag.get(), "Persisted hashtag does not match")

        //Check the relation between the kweet and mentioned user
        assertTrue(foundKweet.get().mentions.contains(foundSnickers.get()), "Kweet doesn't mention SnickersNL")
        assertTrue(foundSnickers.get().mentionedIn.contains(foundKweet.get()), "SnickersNL isn't mentioned")

        //Check the relation between the kweet and the hashtag
        assertTrue(foundKweet.get().hashtags.contains(foundHashtag.get()), "Kweet doesn't use #hardgaan")
        assertTrue(foundHashtag.get().usedBy.contains(foundKweet.get()), "#hardgaan isn't used")
    }

    @Test
    @Transactional
    fun rekweet() {
        val testRekweet = Rekweet(dennis, testKweet)
        rekweetService.save(testRekweet)

        assertEquals(1L, rekweetService.count())
        rekweetService.findById(testRekweet.timelineableId).scopeIfPresentElseFail("Rekweet not found") { rekweet ->
            assertEquals(testRekweet, rekweet, "Rekweet does not match")
        }

        assertEquals(1L, rekweetService.countByKweet(testKweet))
        rekweetService.findByUser(dennis).scopeForEach { rekweet ->
            assertEquals(testRekweet, rekweet, "Rekweet does not match")
        }
    }

    @Test
    @Transactional
    fun like() {
        val testLike = Like(thijs, testKweet)
        likeService.save(testLike)

        assertEquals(1L, likeService.count())
        likeService.findById(testLike.timelineableId).scopeIfPresentElseFail("Like not found") { like ->
            assertEquals(testLike, like, "Likes does not match")
        }

        assertEquals(1L, likeService.countByKweet(testKweet))
        likeService.findByUser(thijs).scopeForEach { like ->
            assertEquals(testLike, like, "Likes does not match")
        }
    }
}