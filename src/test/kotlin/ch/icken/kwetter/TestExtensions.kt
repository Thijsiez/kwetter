package ch.icken.kwetter

import java.util.*
import kotlin.test.fail

inline fun <T> Optional<T>.scopeIfPresentElseFail(failMessage: String = "", block: (T) -> Unit) {
    if (isPresent) get().scope(block) else fail(failMessage)
}
inline fun <T> Iterable<Optional<T>>.scopeIfAllPresentElseFail(failMessage: String = "", block: (List<T>) -> Unit) {
    if (all { it.isPresent }) block(map { it.get() }) else fail(failMessage)
}
inline fun <T> T?.scopeIfNotNullElseFail(failMessage: String = "", block: (T) -> Unit) {
    if (this != null) block(this) else fail(failMessage)
}