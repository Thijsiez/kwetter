package ch.icken.kwetter

import ch.icken.kwetter.domain.Media
import ch.icken.kwetter.domain.User
import ch.icken.kwetter.service.MediaService
import ch.icken.kwetter.service.UserService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.test.*

@RunWith(SpringRunner::class)
@SpringBootTest
class UserTests {
    companion object TestData {
        internal val doNotUseThisThijs = User("Thijsiez", "Thijs Koppen")
        internal val doNotUseThisDennis = User("MachoMuchacho", "Dennis Koeter")
    }

    @Autowired private lateinit var userService: UserService
    @Autowired private lateinit var mediaService: MediaService

    private lateinit var thijs: User
    private lateinit var dennis: User

    @Before
    @Transactional
    fun insertTestData() {
        userService.deleteAll()
        mediaService.deleteAll()

        thijs = userService.save(doNotUseThisThijs)
        dennis = userService.save(doNotUseThisDennis)
    }

    @Test
    @Transactional
    fun countUsers() {
        assertEquals(2L, userService.count())
        userService.findAll().forEach { println(it) }
    }

    @Test
    @Transactional
    fun username() {
        assertFails { User("Rapper Sjors") }

        val sjors = User("RapperSjors")
        assertFails { sjors.username = "Rapper Sjors" }

        userService.findById(thijs.userId).scopeIfPresentElseFail("User not found") { thijs ->
            val oldUsername = thijs.username

            thijs.username = "Jizz_aka_Dixaq"
            userService.save(thijs).scope { updatedThijs ->
                val newUsername = updatedThijs.username
                assertNotEquals(oldUsername, newUsername, "Username was the same")
            }
        }
    }

    @Test
    @Transactional
    fun fullName() {
        userService.findById(dennis.userId).scopeIfPresentElseFail("User not found") { dennis ->
            val oldFullName = dennis.fullName

            dennis.fullName = "Deniz Navidad"
            userService.save(dennis).scope { updatedDennis ->
                val newFullName = updatedDennis.fullName
                assertNotEquals(oldFullName, newFullName, "Full name was the same")
            }
        }
    }

    @Test
    @Transactional
    fun dateOfBirth() {
        //TODO implement a better time API
        userService.findById(thijs.userId).scopeIfPresentElseFail("User not found") { thijs ->
            val oldDateOfBirth = thijs.dateOfBirth

            //TODO maybe add a test for an age-gate feature

            @Suppress("DEPRECATION")
            thijs.dateOfBirth = Date(94, 9, 13)
            userService.save(thijs).scope { updatedThijs ->
                val newDateOfBirth = updatedThijs.dateOfBirth
                assertNotEquals(oldDateOfBirth, newDateOfBirth, "Date of birth was the same")
            }
        }
    }

    @Test
    @Transactional
    fun profileState() {
        userService.findById(dennis.userId).scopeIfPresentElseFail("User not found") { dennis ->
            val oldState = dennis.profileState

            dennis.profileState = User.ProfileState.INACTIVE
            userService.save(dennis).scope { updatedDennis ->
                val newState = updatedDennis.profileState
                assertNotEquals(oldState, newState, "Profile state was the same")
            }
        }
    }
    
    //TODO implement a test to check profile state impact
    //for example if the profile is shown or not
    //if a profile is not ACTIVE, it should not be visible
    //nor should kweets mentioning this profile link to it

    @Test
    @Transactional
    fun verifiedStatus() {
        userService.findById(thijs.userId).scopeIfPresentElseFail("User not found") { thijs ->
            val oldStatus = thijs.verified

            thijs.verified = true
            userService.save(thijs).scope { updatedThijs ->
                val newStatus = updatedThijs.verified
                assertNotEquals(oldStatus, newStatus, "Verified status was the same")
            }
        }
    }

    @Test
    @Transactional
    fun profilePicture() {
        userService.findById(dennis.userId).scopeIfPresentElseFail("User not found") { dennis ->
            val oldPicture = dennis.profilePicture

            val profilePicture = Media("http://i0.kym-cdn.com/entries/icons/facebook/000/000/260/youjustlost.jpg")
            dennis.profilePicture = mediaService.save(profilePicture)

            userService.save(dennis).scope { updatedDennis ->
                val newPicture = updatedDennis.profilePicture
                assertNotEquals(oldPicture?.url, newPicture?.url, "Profile picture was the same")
            }
        }
    }

    @Test
    @Transactional
    fun biography() {
        userService.findById(thijs.userId).scopeIfPresentElseFail("User not found") { thijs ->
            val oldBiography = thijs.biography

            thijs.biography = "Hej, mijn naam is Thijs en ik ga lekker op drum 'n bass"
            userService.save(thijs).scope { updatedThijs ->
                val newBiography = updatedThijs.biography
                assertNotEquals(oldBiography, newBiography, "Biography was the same")
            }
        }
    }

    @Test
    @Transactional
    fun startAndStopFollowing() {
        val foundUsers = userService.findByIds(thijs.userId, dennis.userId)
        foundUsers.scopeIfAllPresentElseFail("Users not found") { (thijs, dennis) ->
            userService.startFollowing(thijs, dennis)
            userService.save(thijs, dennis).scope { (updatedThijs, updatedDennis) ->
                assertTrue(updatedThijs.following.contains(dennis), "Thijs does not follow Dennis")
                assertTrue(updatedDennis.followers.contains(thijs), "Dennis is not followed by Thijs")
            }

            userService.stopFollowing(thijs, dennis)
            userService.save(thijs, dennis).scope { (updatedThijs, updatedDennis) ->
                assertFalse(updatedThijs.following.contains(dennis), "Thijs follows Dennis")
                assertFalse(updatedDennis.followers.contains(thijs), "Dennis is followed by Thijs")
            }
        }
    }
}